// import org.springframework.core.ParameterizedTypeReference;
// import org.springframework.http.ResponseEntity;
// import org.springframework.web.client.RestTemplate;
// import java.util.List;

// public class BookRankingClient {
//     private static final String BASE_URL = "http://localhost:3000/books/rank";

//     public static void main(String[] args) {
//         RestTemplate restTemplate = new RestTemplate();
//         ResponseEntity<List<BookRanking>> response = restTemplate.exchange(
//             BASE_URL,
//             HttpMethod.GET,
//             null,
//             new ParameterizedTypeReference<List<BookRanking>>() {}
//         );
//         List<BookRanking> rankedBooks = response.getBody();

//         // Print ranked books
//         if (rankedBooks != null) {
//             for (BookRanking book : rankedBooks) {
//                 System.out.println("Book ID: " + book.getBookId() + ", Average Score: " + book.getAverageScore());
//             }
//         } else {
//             System.out.println("No ranked books found.");
//         }
//     }
// }

// class BookRanking {
//     private int book_id;
//     private double average_score;

//     public int getBookId() {
//         return book_id;
//     }

//     public double getAverageScore() {
//         return average_score;
//     }
// }