import { Test, TestingModule } from '@nestjs/testing';
import { ReportFeedsController } from './report_feeds.controller';
import { ReportFeedsService } from './report_feeds.service';

describe('ReportFeedsController', () => {
  let controller: ReportFeedsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportFeedsController],
      providers: [ReportFeedsService],
    }).compile();

    controller = module.get<ReportFeedsController>(ReportFeedsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
