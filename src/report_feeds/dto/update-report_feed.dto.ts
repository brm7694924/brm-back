import { PartialType } from '@nestjs/mapped-types';
import { CreateReportFeedDto } from './create-report_feed.dto';

export class UpdateReportFeedDto extends PartialType(CreateReportFeedDto) {}
