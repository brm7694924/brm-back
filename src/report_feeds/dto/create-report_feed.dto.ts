import { IsNotEmpty } from 'class-validator';

export class CreateReportFeedDto {
  @IsNotEmpty()
  feed_id: number;
}
