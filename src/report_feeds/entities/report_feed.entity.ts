import { FeedComment } from 'src/feed_comments/entities/feed_comment.entity';
import { Feed } from 'src/feeds/entities/feed.entity';
import { Review } from 'src/reviews/entities/review.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class ReportFeed {
  @PrimaryGeneratedColumn()
  rep_feed_id: number;

  @ManyToOne(() => Feed, (feed) => feed.reportFeed)
  feed: Feed;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
