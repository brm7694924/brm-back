import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReportFeedDto } from './dto/create-report_feed.dto';
import { UpdateReportFeedDto } from './dto/update-report_feed.dto';
import { ReportFeed } from './entities/report_feed.entity';
import { Feed } from 'src/feeds/entities/feed.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ReportFeedsService {
  constructor(
    @InjectRepository(ReportFeed)
    private reportFeedsRepository: Repository<ReportFeed>,
    @InjectRepository(Feed)
    private feedsRepository: Repository<Feed>,
  ) {}
  async create(createReportFeedDto: CreateReportFeedDto) {
    const feed = await this.feedsRepository.findOne({
      where: { feed_id: createReportFeedDto.feed_id },
    });
    const newReportFeed = new ReportFeed();
    newReportFeed.feed = feed;
    return this.reportFeedsRepository.save(newReportFeed);
  }

  findByFeed(FeedId: number) {
    return this.reportFeedsRepository.find({
      where: { feed: { feed_id: FeedId } },
      relations: ['feed'],
    });
  }

  findAll() {
    return this.reportFeedsRepository.find({ relations: ['feed'] });
  }

  findOne(id: number) {
    const reportFeed = this.reportFeedsRepository.findOne({
      where: { rep_feed_id: id },
      relations: ['feed'],
    });
    if (!reportFeed) {
      throw new NotFoundException();
    }
    return this.reportFeedsRepository.findOne({ where: { rep_feed_id: id } });
  }

  async update(id: number, updateReportFeedDto: UpdateReportFeedDto) {
    const reportFeed = await this.reportFeedsRepository.findOneBy({
      rep_feed_id: id,
    });
    if (!reportFeed) {
      throw new NotFoundException();
    }
    const updatedReportFeed = { ...reportFeed, ...updateReportFeedDto };
    return this.reportFeedsRepository.save(updatedReportFeed);
  }

  async remove(id: number) {
    const reportFeed = await this.reportFeedsRepository.findOneBy({
      rep_feed_id: id,
    });
    if (!reportFeed) {
      throw new NotFoundException();
    }
    return this.reportFeedsRepository.softRemove(reportFeed);
  }
}
