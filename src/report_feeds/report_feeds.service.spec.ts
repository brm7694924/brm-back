import { Test, TestingModule } from '@nestjs/testing';
import { ReportFeedsService } from './report_feeds.service';

describe('ReportFeedsService', () => {
  let service: ReportFeedsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportFeedsService],
    }).compile();

    service = module.get<ReportFeedsService>(ReportFeedsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
