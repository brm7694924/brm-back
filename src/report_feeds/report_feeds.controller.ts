import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ReportFeedsService } from './report_feeds.service';
import { CreateReportFeedDto } from './dto/create-report_feed.dto';
import { UpdateReportFeedDto } from './dto/update-report_feed.dto';

@Controller('report_feeds')
export class ReportFeedsController {
  constructor(private readonly reportFeedsService: ReportFeedsService) {}

  @Post()
  create(@Body() createReportFeedDto: CreateReportFeedDto) {
    return this.reportFeedsService.create(createReportFeedDto);
  }

  @Get()
  findAll() {
    return this.reportFeedsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reportFeedsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateReportFeedDto: UpdateReportFeedDto,
  ) {
    return this.reportFeedsService.update(+id, updateReportFeedDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reportFeedsService.remove(+id);
  }
}
