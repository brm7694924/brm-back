import { Module } from '@nestjs/common';
import { ReportFeedsService } from './report_feeds.service';
import { ReportFeedsController } from './report_feeds.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Feed } from 'src/feeds/entities/feed.entity';
import { FeedsModule } from 'src/feeds/feeds.module';
import { ReportFeed } from './entities/report_feed.entity';

@Module({
  imports: [FeedsModule, TypeOrmModule.forFeature([ReportFeed, Feed])],
  controllers: [ReportFeedsController],
  providers: [ReportFeedsService],
  exports: [ReportFeedsModule],
})
export class ReportFeedsModule {}
