import { Test, TestingModule } from '@nestjs/testing';
import { MyfavoritesController } from './myfavorites.controller';
import { MyfavoritesService } from './myfavorites.service';

describe('MyfavoritesController', () => {
  let controller: MyfavoritesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MyfavoritesController],
      providers: [MyfavoritesService],
    }).compile();

    controller = module.get<MyfavoritesController>(MyfavoritesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
