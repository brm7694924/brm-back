import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMyfavoriteDto } from './dto/create-myfavorite.dto';
import { UpdateMyfavoriteDto } from './dto/update-myfavorite.dto';
import { Myfavorite } from './entities/myfavorite.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Book } from 'src/books/entities/book.entity';

@Injectable()
export class MyfavoritesService {
  constructor(
    @InjectRepository(Myfavorite)
    private myfavoritesRepository: Repository<Myfavorite>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
  ) {}

  async create(createMyfavoriteDto: CreateMyfavoriteDto) {
    const user = await this.usersRepository.findOne({
      where: { user_id: createMyfavoriteDto.user_id },
    });
    const book = await this.booksRepository.findOne({
      where: { book_id: createMyfavoriteDto.book_id },
    });
    if (!user || !book) {
      throw new NotFoundException('User or Book not found');
    }
    const newMyfavorite = new Myfavorite();
    newMyfavorite.user = user;
    newMyfavorite.book = [book];
    return this.myfavoritesRepository.save(newMyfavorite);
  }

  findByUser(userId: number) {
    return this.myfavoritesRepository.find({
      where: { user: { user_id: userId } },
      relations: ['user', 'book'],
    });
  }

  findAll() {
    return this.myfavoritesRepository.find({ relations: ['user', 'book'] });
  }

  async findOne(id: number) {
    const myfavorite = await this.myfavoritesRepository.findOne({
      where: { myfav_id: id },
      relations: ['user', 'book'],
    });
    if (!myfavorite) {
      throw new NotFoundException();
    }
    return myfavorite;
  }

  async getFavoriteBooksByUserId(id: number): Promise<Myfavorite[]> {
    return this.myfavoritesRepository.find({
      where: { user: { user_id: id } },
      relations: ['user', 'book'],
    });
  }

  // async getUserFavorite(userId: number): Promise<any> {
  //   const books = await this.booksRepository
  //     .createQueryBuilder('book')
  //     .leftJoinAndSelect('book.myfavorites', 'myfavorite')
  //     .innerJoinAndSelect('myfavorite.user', 'user')
  //     .where('user.user_id = :id', { id: userId })
  //     .getMany();

  //   if (!books.length) {
  //     throw new NotFoundException(
  //       `No favorite books found for user ID ${userId}`,
  //     );
  //   }

  //   // สร้างโครงสร้างข้อมูลที่ต้องการ
  //   const userFavoriteBooks = books.map((book) => {
  //     const { myfavorite, ...bookDetails } = book;
  //     const users = (myfavorite as Myfavorite[]).map((mf) => mf.user); // ข้อมูลผู้ใช้ทั้งหมดที่ชื่นชอบหนังสือ

  //     return {
  //       ...bookDetails,
  //       users: users.map((user) => ({
  //         user_id: user.user_id,
  //         user_name: user.user_name,
  //       })),
  //     };
  //   });

  //   // ใช้ Set เพื่อลดข้อมูลผู้ใช้ที่ซ้ำกัน
  //   const users = userFavoriteBooks.flatMap((b) => b.users);
  //   const uniqueUsers = Array.from(new Set(users.map((u) => u.user_id))).map(
  //     (user_id) => ({
  //       user_id,
  //       user_name: users.find((u) => u.user_id === user_id)?.user_name,
  //     }),
  //   );

  //   // สร้างข้อมูลที่จัดกลุ่มตามผู้ใช้
  //   const groupedResult = uniqueUsers.map((user) => {
  //     return {
  //       user_id: user.user_id,
  //       user_name: user.user_name,
  //       books: userFavoriteBooks
  //         .filter((book) => book.users.some((u) => u.user_id === user.user_id))
  //         .map((b) => ({
  //           book_id: b.book_id,
  //           book_title: b.book_title,
  //         })),
  //     };
  //   });

  //   return groupedResult;
  // }

  async update(id: number, updateMyfavoriteDto: UpdateMyfavoriteDto) {
    const myfavorite = await this.myfavoritesRepository.findOneBy({
      myfav_id: id,
    });
    if (!myfavorite) {
      throw new NotFoundException();
    }
    const updatedMyfavorite = { ...myfavorite, ...updateMyfavoriteDto };
    return this.myfavoritesRepository.save(updatedMyfavorite);
  }

  async remove(id: number) {
    const myfavorite = await this.myfavoritesRepository.findOneBy({
      myfav_id: id,
    });
    if (!myfavorite) {
      throw new NotFoundException();
    }
    return this.myfavoritesRepository.softRemove(myfavorite);
  }
}
