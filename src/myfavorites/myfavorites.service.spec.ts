import { Test, TestingModule } from '@nestjs/testing';
import { MyfavoritesService } from './myfavorites.service';

describe('MyfavoritesService', () => {
  let service: MyfavoritesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MyfavoritesService],
    }).compile();

    service = module.get<MyfavoritesService>(MyfavoritesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
