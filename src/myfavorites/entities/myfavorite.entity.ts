import { Book } from 'src/books/entities/book.entity';
import { FeedComment } from 'src/feed_comments/entities/feed_comment.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
  Unique,
  OneToOne,
} from 'typeorm';

@Entity()
// @Unique(['user'])
export class Myfavorite {
  @PrimaryGeneratedColumn()
  myfav_id: number;

  @ManyToOne(() => User, (user) => user.myfavorite)
  user: User;

  @OneToMany(() => Book, (book) => book.myfavorite)
  book: Book[];

  // @OneToOne(() => Book, (book) => book.myfavorite)
  // book: Book;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
