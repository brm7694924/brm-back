import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { MyfavoritesService } from './myfavorites.service';
import { CreateMyfavoriteDto } from './dto/create-myfavorite.dto';
import { UpdateMyfavoriteDto } from './dto/update-myfavorite.dto';

@Controller('myfavorites')
export class MyfavoritesController {
  constructor(private readonly myfavoritesService: MyfavoritesService) {}

  @Post()
  create(@Body() createMyfavoriteDto: CreateMyfavoriteDto) {
    return this.myfavoritesService.create(createMyfavoriteDto);
  }

  @Get()
  findAll() {
    return this.myfavoritesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.myfavoritesService.findOne(+id);
  }

  @Get(':userId/books/:id')
  async getFavoriteBooks(@Param('id') id: number) {
    return this.myfavoritesService.getFavoriteBooksByUserId(id);
  }

  // @Get('user/:id/favorites')
  // async getUserFavorite(@Param('id') id: string) {
  //   return this.myfavoritesService.getUserFavorite(+id);
  // }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMyfavoriteDto: UpdateMyfavoriteDto,
  ) {
    return this.myfavoritesService.update(+id, updateMyfavoriteDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.myfavoritesService.remove(+id);
  }
}
