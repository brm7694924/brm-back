import { Module } from '@nestjs/common';
import { MyfavoritesService } from './myfavorites.service';
import { MyfavoritesController } from './myfavorites.controller';
import { Myfavorite } from './entities/myfavorite.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { UsersModule } from 'src/users/users.module';
import { Book } from 'src/books/entities/book.entity';

@Module({
  imports: [UsersModule, TypeOrmModule.forFeature([Myfavorite, User, Book])],
  controllers: [MyfavoritesController],
  providers: [MyfavoritesService],
  exports: [MyfavoritesModule],
})
export class MyfavoritesModule {}
