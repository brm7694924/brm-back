import { IsNotEmpty } from 'class-validator';

export class CreateMyfavoriteDto {
  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  book_id: number;
}
