import { PartialType } from '@nestjs/mapped-types';
import { CreateMyfavoriteDto } from './create-myfavorite.dto';

export class UpdateMyfavoriteDto extends PartialType(CreateMyfavoriteDto) {}
