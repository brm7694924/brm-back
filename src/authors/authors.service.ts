import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Author } from './entities/author.entity';
import { Book } from 'src/books/entities/book.entity';

@Injectable()
export class AuthorsService {
  constructor(
    @InjectRepository(Author)
    private authorsRepository: Repository<Author>,
  ) {}
  async create(createAuthorDto: CreateAuthorDto) {
    const newAuthor = new Author();
    newAuthor.author_name = createAuthorDto.author_name;
    return this.authorsRepository.save(newAuthor);
  }

  findAll() {
    return this.authorsRepository.find({});
  }

  async findOne(id: number) {
    const author = await this.authorsRepository.findOne({
      where: { author_id: id },
    });
    if (!author) {
      throw new NotFoundException();
    }
    return author;
  }

  async update(id: number, updateAuthorDto: UpdateAuthorDto) {
    const author = await this.authorsRepository.findOneBy({ author_id: id });
    if (!author) {
      throw new NotFoundException();
    }
    const updatedAuthor = { ...author, ...updateAuthorDto };
    return this.authorsRepository.save(updatedAuthor);
  }

  async remove(id: number) {
    const author = await this.authorsRepository.findOneBy({ author_id: id });
    if (!author) {
      throw new NotFoundException();
    }
    return this.authorsRepository.softRemove(author);
  }

  async findAuthorsByBook(bookId: number): Promise<any> {
    const authors = await this.authorsRepository
      .createQueryBuilder('author')
      .leftJoinAndSelect('author.bookAuthor', 'bookAuthor')
      .innerJoinAndSelect('bookAuthor.book', 'book')
      .where('book.book_id = :id', { id: bookId }) // ค้นหาตาม bookId
      .getMany();

    if (!authors.length) {
      throw new NotFoundException(`No authors found for book ID ${bookId}`); // เปลี่ยนข้อความให้ถูกต้อง
    }

    // สร้างโครงสร้างข้อมูลที่ต้องการ
    const authorBooks = authors.map((author) => {
      const { bookAuthor, ...authorDetails } = author; // แยก bookAuthor ออกไป
      const books = (bookAuthor as { book: Book }[]).map(
        // เปลี่ยนจาก categories เป็น books
        (ba) => ba.book,
      ); // ข้อมูลหนังสือทั้งหมดที่เกี่ยวข้องกับผู้เขียน

      return {
        ...authorDetails,
        books: books.map((book) => ({
          book_id: book.book_id,
          book_title: book.book_title,
        })),
      };
    });

    // ใช้ Set เพื่อลดข้อมูลหนังสือที่ซ้ำกัน
    const uniqueBooks = Array.from(
      new Set(authorBooks.flatMap((b) => b.books.map((book) => book.book_id))),
    ).map((book_id) => {
      const firstMatch = authorBooks
        .flatMap((b) => b.books)
        .find((book) => book.book_id === book_id);
      return {
        book_id,
        book_title: firstMatch?.book_title, // เปลี่ยนให้ตรงกับโครงสร้างข้อมูล
      };
    });

    // สร้างข้อมูลที่จัดกลุ่มตามผู้เขียน
    const groupedResult = uniqueBooks.map((book) => {
      return {
        book_id: book.book_id,
        book_title: book.book_title,
        authors: authorBooks
          .filter((author) =>
            author.books.some((b) => b.book_id === book.book_id),
          )
          .map((a) => ({
            author_id: a.author_id,
            author_name: a.author_name,
          })),
      };
    });

    return groupedResult;
  }
}
