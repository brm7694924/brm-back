import { BookAuthor } from 'src/book_author/entities/book_author.entity';
import { Book } from 'src/books/entities/book.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';

@Entity()
export class Author {
  @PrimaryGeneratedColumn()
  author_id: number;

  @Column({})
  author_name: string;

  @OneToMany(() => BookAuthor, (book_author) => book_author.author)
  bookAuthor: [];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
