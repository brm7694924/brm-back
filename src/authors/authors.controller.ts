import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';

@Controller('authors')
export class AuthorsController {
  constructor(private readonly authorsService: AuthorsService) {}

  @Post()
  async create(@Body() createAuthorDto: CreateAuthorDto) {
    try {
      return await this.authorsService.create(createAuthorDto);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException(error.message);
      }
      throw new BadRequestException('Failed to create author');
    }
  }

  @Get()
  async findAll() {
    try {
      return await this.authorsService.findAll();
    } catch (error) {
      throw new BadRequestException('Failed to fetch authors');
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      return await this.authorsService.findOne(+id);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException('Author not found');
      }
      throw new BadRequestException('Failed to fetch author');
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateAuthorDto: UpdateAuthorDto,
  ) {
    try {
      return await this.authorsService.update(+id, updateAuthorDto);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException('Author not found');
      }
      throw new BadRequestException('Failed to update author');
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      return await this.authorsService.remove(+id);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException('Author not found');
      }
      throw new BadRequestException('Failed to delete author');
    }
  }

  @Get('book/:id')
  async getAuthorsByBook(@Param('id') id: string) {
    return this.authorsService.findAuthorsByBook(+id);
  }
}
