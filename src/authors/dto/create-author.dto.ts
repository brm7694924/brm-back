import { IsArray, IsNotEmpty } from 'class-validator';

export class CreateAuthorDto {
  @IsNotEmpty()
  author_name: string;
}
