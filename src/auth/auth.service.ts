import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from 'src/users/entities/user.entity';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async register(createUserDto: CreateUserDto) {
    const hashedPassword = await bcrypt.hash(createUserDto.user_password, 10); // แฮชรหัสผ่าน
    return this.usersService.create({
      ...createUserDto,
      user_password: hashedPassword,
    });
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(username); // ดึงผู้ใช้จากฐานข้อมูล
    if (!user) {
      return null; // หากไม่พบผู้ใช้คืนค่า null
    }

    const isMatch = await bcrypt.compare(pass, user.user_password); // ตรวจสอบรหัสผ่าน
    if (isMatch) {
      const { user_password, ...result } = user;
      return result; // ส่งคืนข้อมูลผู้ใช้หากรหัสผ่านถูกต้อง
    }
    return null;
  }

  async login(user: User) {
    console.log('User data:', user);
    const payload = { username: user.user_email, sub: user.user_id };
    return {
      access_token: this.jwtService.sign(payload),
      user: user,
    };
  }
}
