import { Controller, Post, Body, Request, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { LocalAuthGuard } from './local-auth.guard';
import { JwtAuthGuard } from './jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  async register(@Body() createUserDto: CreateUserDto) {
    try {
      const user = await this.authService.register(createUserDto);
      return { success: true, message: 'User registered successfully', user };
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        // ตัวอย่างการตรวจสอบข้อผิดพลาด
        return { success: false, message: 'อีเมล์ที่ลงทะเบียนไว้แล้ว' };
      }
      return { success: false, message: 'เกิดข้อผิดพลาดในการลงทะเบียน' };
    }
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
