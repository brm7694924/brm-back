import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BooksModule } from './books/books.module';
import { DataSource } from 'typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { Book } from './books/entities/book.entity';
import { CategoriesModule } from './categories/categories.module';
import { Category } from './categories/entities/category.entity';
import { AuthorsModule } from './authors/authors.module';
import { Author } from './authors/entities/author.entity';
import { FeedsModule } from './feeds/feeds.module';
import { Feed } from './feeds/entities/feed.entity';
import { ReviewsModule } from './reviews/reviews.module';
import { Review } from './reviews/entities/review.entity';
import { ReportsModule } from './reports/reports.module';
import { Report } from './reports/entities/report.entity';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { FeedCommentsModule } from './feed_comments/feed_comments.module';
import { FeedComment } from './feed_comments/entities/feed_comment.entity';
import { MyfavoritesModule } from './myfavorites/myfavorites.module';
import { Myfavorite } from './myfavorites/entities/myfavorite.entity';
import { ReportReviewsModule } from './report_reviews/report_reviews.module';
import { ReportReview } from './report_reviews/entities/report_review.entity';
import { ReportFeedsModule } from './report_feeds/report_feeds.module';
import { ReportFeed } from './report_feeds/entities/report_feed.entity';
import { AuthModule } from './auth/auth.module';
import { BookAuthorModule } from './book_author/book_author.module';
import { BookCategoryModule } from './book_category/book_category.module';
import { BookAuthor } from './book_author/entities/book_author.entity';
import { BookCategory } from './book_category/entities/book_category.entity';
import { join } from 'path';
import { SentimentsModule } from './sentiments/sentiments.module';
import { Sentiment } from './sentiments/entities/sentiment.entity';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      renderPath: 'public',
    }),
    TypeOrmModule.forRoot({
      // ลิ้งค์ database http://localhost/phpmyadmin อย่าลืมใช้ xampp ด้วยนะ
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'book_review_db',
      entities: [
        Book,
        Category,
        Author,
        BookAuthor,
        BookCategory,
        Feed,
        Review,
        Report,
        User,
        Role,
        FeedComment,
        Myfavorite,
        ReportReview,
        ReportFeed,
        Sentiment,
      ],
      synchronize: true,
    }),
    BooksModule,
    CategoriesModule,
    AuthorsModule,
    ReviewsModule,
    ReportsModule,
    UsersModule,
    RolesModule,
    FeedCommentsModule,
    MyfavoritesModule,
    ReportReviewsModule,
    FeedsModule,
    ReportFeedsModule,
    AuthModule,
    BookAuthorModule,
    BookCategoryModule,
    SentimentsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
