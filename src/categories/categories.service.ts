import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { Repository } from 'typeorm';
import { Book } from 'src/books/entities/book.entity';

@Injectable()
export class CategoriesService {
  booksRepository: any;
  constructor(
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) {}
  create(createCategoryDto: CreateCategoryDto) {
    return this.categoriesRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categoriesRepository.find();
  }

  async findOne(id: number) {
    const category = await this.categoriesRepository.findOne({
      where: { category_id: id },
    });
    if (!category) {
      throw new NotFoundException();
    }
    return this.categoriesRepository.findOne({ where: { category_id: id } });
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const category = await this.categoriesRepository.findOneBy({
      category_id: id,
    });
    if (!category) {
      throw new NotFoundException();
    }
    const updatedCategory = { ...category, ...updateCategoryDto };
    return this.categoriesRepository.save(updatedCategory);
  }

  async remove(id: number) {
    const category = await this.categoriesRepository.findOneBy({
      category_id: id,
    });
    if (!category) {
      throw new NotFoundException();
    }
    return this.categoriesRepository.softRemove(category);
  }

  async findCategoriesByBook(bookId: number): Promise<any> {
    const categories = await this.categoriesRepository
      .createQueryBuilder('category')
      .leftJoinAndSelect('category.bookCategory', 'bookCategory')
      .innerJoinAndSelect('bookCategory.book', 'book')
      .where('book.book_id = :id', { id: bookId }) // เปลี่ยนตรงนี้ให้ตรงกับการค้นหาตาม bookId
      .getMany();

    if (!categories.length) {
      throw new NotFoundException(`No categories found for book ID ${bookId}`); // เปลี่ยนข้อความให้ถูกต้อง
    }

    // สร้างโครงสร้างข้อมูลที่ต้องการ
    const categoryBooks = categories.map((category) => {
      // เปลี่ยนจาก books เป็น categories
      const { bookCategory, ...categoryDetails } = category; // แยก bookCategory ออกไป
      const books = (bookCategory as { book: Book }[]).map(
        // เปลี่ยนจาก categories เป็น books
        (bc) => bc.book,
      ); // ข้อมูลหนังสือทั้งหมดในหมวดหมู่

      return {
        ...categoryDetails,
        books: books.map((book) => ({
          // เปลี่ยนจาก categories เป็น books
          book_id: book.book_id,
          book_title: book.book_title,
        })),
      };
    });

    // ใช้ Set เพื่อลดข้อมูลหมวดหมู่ที่ซ้ำกัน
    const uniqueCategories = Array.from(
      new Set(
        categoryBooks.flatMap((b) => b.books.map((book) => book.book_id)),
      ), // แก้ไขตรงนี้
    ).map((book_id) => {
      const firstMatch = categoryBooks
        .flatMap((b) => b.books)
        .find((book) => book.book_id === book_id);
      return {
        book_id,
        book_title: firstMatch?.book_title, // เปลี่ยนให้ตรงกับโครงสร้างข้อมูล
      };
    });

    // สร้างข้อมูลที่จัดกลุ่มตามหมวดหมู่
    const groupedResult = uniqueCategories.map((book) => {
      return {
        book_id: book.book_id,
        book_title: book.book_title,
        categories: categoryBooks
          .filter((category) =>
            category.books.some((b) => b.book_id === book.book_id),
          )
          .map((c) => ({
            category_id: c.category_id,
            category_name: c.category_name,
          })),
      };
    });

    return groupedResult;
  }
}
