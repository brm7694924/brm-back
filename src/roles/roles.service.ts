import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
  ) {}
  async create(createRoleDto: CreateRoleDto) {
    const newRole = new Role();
    newRole.role_name = createRoleDto.role_name;
    return this.rolesRepository.save(newRole);
  }
  findAll() {
    return this.rolesRepository.find();
  }

  async findOne(id: number) {
    const role = await this.rolesRepository.findOne({
      where: { role_id: id },
    });
    if (!role) {
      throw new NotFoundException();
    }
    return this.rolesRepository.findOne({ where: { role_id: id } });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    const role = await this.rolesRepository.findOneBy({ role_id: id });
    if (!role) {
      throw new NotFoundException();
    }
    const updatedRole = { ...role, ...updateRoleDto };
    return this.rolesRepository.save(updatedRole);
  }

  async remove(id: number) {
    const role = await this.rolesRepository.findOneBy({ role_id: id });
    if (!role) {
      throw new NotFoundException();
    }
    return this.rolesRepository.softRemove(role);
  }
}
