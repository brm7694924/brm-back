import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BookAuthorService } from './book_author.service';
import { CreateBookAuthorDto } from './dto/create-book_author.dto';
import { UpdateBookAuthorDto } from './dto/update-book_author.dto';

@Controller('book_author')
export class BookAuthorController {
  constructor(private readonly bookAuthorService: BookAuthorService) {}

  @Post()
  create(@Body() createBookAuthorDto: CreateBookAuthorDto) {
    return this.bookAuthorService.create(createBookAuthorDto);
  }

  @Get()
  findAll() {
    return this.bookAuthorService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bookAuthorService.findOne(+id);
  }

  @Get('author/:author_id')
  findByAuthor(@Param('author_id') author_id: number) {
    return this.bookAuthorService.findByAuthor(author_id);
  }

  @Get('book/:book_id')
  findByBook(@Param('book_id') book_id: number) {
    return this.bookAuthorService.findByBook(book_id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBookAuthorDto: UpdateBookAuthorDto,
  ) {
    return this.bookAuthorService.update(+id, updateBookAuthorDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bookAuthorService.remove(+id);
  }
}
