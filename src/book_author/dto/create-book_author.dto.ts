import { IsNotEmpty } from 'class-validator';
export class CreateBookAuthorDto {
  @IsNotEmpty()
  author_id: number;

  @IsNotEmpty()
  book_id: number;
}
