import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBookAuthorDto } from './dto/create-book_author.dto';
import { UpdateBookAuthorDto } from './dto/update-book_author.dto';
import { BookAuthor } from './entities/book_author.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Author } from 'src/authors/entities/author.entity';
import { Book } from 'src/books/entities/book.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BookAuthorService {
  constructor(
    @InjectRepository(BookAuthor)
    private bookAuthorRepository: Repository<BookAuthor>,
    @InjectRepository(Author)
    private authorsRepository: Repository<Author>,
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
  ) {}

  async create(createBookAuthorDto: CreateBookAuthorDto) {
    const author = await this.authorsRepository.findOne({
      where: { author_id: createBookAuthorDto.author_id },
    });
    const book = await this.booksRepository.findOne({
      where: { book_id: createBookAuthorDto.book_id },
    });
    const newBookAut = new BookAuthor();
    newBookAut.author = author;
    newBookAut.book = book;
    return this.bookAuthorRepository.save(newBookAut);
  }

  findAll() {
    return this.bookAuthorRepository.find({ relations: ['author', 'book'] });
  }

  findByAuthor(author_id: number) {
    return this.bookAuthorRepository.find({
      where: { author_id: author_id },
      relations: { author: true },
      order: { name: 'ASC' },
    });
  }

  findByBook(book_id: number) {
    return this.bookAuthorRepository.find({
      where: { book_id: book_id },
      relations: { book: true },
      order: { name: 'ASC' },
    });
  }

  findOne(id: number) {
    const bookAuthor = this.bookAuthorRepository.findOne({
      where: { bookaut_id: id },
      relations: ['author', 'book'],
    });
    if (!bookAuthor) {
      throw new NotFoundException();
    }
    return this.bookAuthorRepository.findOne({ where: { bookaut_id: id } });
  }

  async update(id: number, updateBookAuthorDto: UpdateBookAuthorDto) {
    const bookAuthor = await this.bookAuthorRepository.findOneBy({
      bookaut_id: id,
    });
    if (!bookAuthor) {
      throw new NotFoundException();
    }
    const updatedBookAuthor = { ...bookAuthor, ...updateBookAuthorDto };
    return this.bookAuthorRepository.save(updatedBookAuthor);
  }

  async remove(id: number) {
    const bookAuthor = await this.bookAuthorRepository.findOneBy({
      bookaut_id: id,
    });
    if (!bookAuthor) {
      throw new NotFoundException();
    }
    return this.bookAuthorRepository.softRemove(bookAuthor);
  }
}
