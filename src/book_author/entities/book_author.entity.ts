import { Author } from 'src/authors/entities/author.entity';
import { Book } from 'src/books/entities/book.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class BookAuthor {
  [x: string]: any;
  @PrimaryGeneratedColumn()
  bookaut_id: number;

  @ManyToOne(() => Author, (author) => author.bookAuthor)
  author: Author;

  @ManyToOne(() => Book, (book) => book.bookAuthor)
  book: Book;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
