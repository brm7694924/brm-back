import { Module } from '@nestjs/common';
import { BookAuthorService } from './book_author.service';
import { BookAuthorController } from './book_author.controller';
import { BookAuthor } from './entities/book_author.entity';
import { AuthorsModule } from 'src/authors/authors.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Author } from 'src/authors/entities/author.entity';
import { BooksModule } from 'src/books/books.module';
import { Book } from 'src/books/entities/book.entity';

@Module({
  imports: [
    AuthorsModule,
    BooksModule,
    TypeOrmModule.forFeature([BookAuthor, Author, Book]),
  ],

  controllers: [BookAuthorController],
  providers: [BookAuthorService],
  exports: [BookAuthorModule],
})
export class BookAuthorModule {}
