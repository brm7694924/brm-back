import { IsNotEmpty } from 'class-validator';

export class CreateBookCategoryDto {
  @IsNotEmpty()
  category_id: number;

  @IsNotEmpty()
  book_id: number;
}
