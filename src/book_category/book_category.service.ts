import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBookCategoryDto } from './dto/create-book_category.dto';
import { UpdateBookCategoryDto } from './dto/update-book_category.dto';
import { BookCategory } from './entities/book_category.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from 'src/books/entities/book.entity';
import { Category } from 'src/categories/entities/category.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BookCategoryService {
  constructor(
    @InjectRepository(BookCategory)
    private bookCategorysRepository: Repository<BookCategory>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
  ) {}

  async create(createBookCategoryDto: CreateBookCategoryDto) {
    const category = await this.categorysRepository.findOne({
      where: { category_id: createBookCategoryDto.category_id },
    });
    const book = await this.booksRepository.findOne({
      where: { book_id: createBookCategoryDto.book_id },
    });
    const newBookCategory = new BookCategory();
    newBookCategory.category = category;
    newBookCategory.book = book;
    return this.bookCategorysRepository.save(newBookCategory);
  }

  findAll() {
    return this.bookCategorysRepository.find({
      relations: ['category', 'book'],
    });
  }

  findByCategory(category_id: number) {
    return this.bookCategorysRepository.find({
      where: { category_id: category_id },
      relations: { category: true },
      order: { name: 'ASC' },
    });
  }

  findByBook(book_id: number) {
    return this.bookCategorysRepository.find({
      where: { book_id: book_id },
      relations: { book: true },
      order: { name: 'ASC' },
    });
  }

  findOne(id: number) {
    const bookcategory = this.bookCategorysRepository.findOne({
      where: { bookcat_id: id },
      relations: ['category', 'book'],
    });
    if (!bookcategory) {
      throw new NotFoundException();
    }
    return this.bookCategorysRepository.findOne({ where: { bookcat_id: id } });
  }

  async update(id: number, updateBookCategoryDto: UpdateBookCategoryDto) {
    const bookCategory = await this.bookCategorysRepository.findOneBy({
      bookcat_id: id,
    });
    if (!bookCategory) {
      throw new NotFoundException();
    }
    const updatedBookCategory = { ...bookCategory, ...updateBookCategoryDto };
    return this.bookCategorysRepository.save(updatedBookCategory);
  }

  async remove(id: number) {
    const bookCategory = await this.bookCategorysRepository.findOneBy({
      bookcat_id: id,
    });
    if (!bookCategory) {
      throw new NotFoundException();
    }
    return this.bookCategorysRepository.softRemove(bookCategory);
  }
}
