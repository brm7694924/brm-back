import { Module } from '@nestjs/common';
import { BookCategoryService } from './book_category.service';
import { BookCategoryController } from './book_category.controller';
import { Category } from 'src/categories/entities/category.entity';
import { CategoriesModule } from 'src/categories/categories.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BooksModule } from 'src/books/books.module';
import { Book } from 'src/books/entities/book.entity';
import { BookCategory } from './entities/book_category.entity';

@Module({
  imports: [
    CategoriesModule,
    BooksModule,
    TypeOrmModule.forFeature([BookCategory, Category, Book]),
  ],

  controllers: [BookCategoryController],
  providers: [BookCategoryService],
  exports: [BookCategoryModule],
})
export class BookCategoryModule {}
