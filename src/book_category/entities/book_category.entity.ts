import { Book } from 'src/books/entities/book.entity';
import { Category } from 'src/categories/entities/category.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class BookCategory {
  [x: string]: any;
  @PrimaryGeneratedColumn()
  bookcat_id: number;

  @ManyToOne(() => Category, (category) => category.bookCategory)
  category: Category;

  @ManyToOne(() => Book, (book) => book.bookCategory)
  book: Book;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
