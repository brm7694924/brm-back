import { Module } from '@nestjs/common';
import { SentimentsService } from './sentiments.service';
import { SentimentsController } from './sentiments.controller';
import { Sentiment } from './entities/sentiment.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Sentiment])],
  controllers: [SentimentsController],
  providers: [SentimentsService],
  exports: [SentimentsModule],
})
export class SentimentsModule {}
