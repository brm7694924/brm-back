import { IsNotEmpty } from 'class-validator';

export class CreateSentimentDto {
  @IsNotEmpty()
  senti_label: string;

  @IsNotEmpty()
  senti_score: number;
}
