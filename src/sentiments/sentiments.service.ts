import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSentimentDto } from './dto/create-sentiment.dto';
import { UpdateSentimentDto } from './dto/update-sentiment.dto';
import { Sentiment } from './entities/sentiment.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SentimentsService {
  constructor(
    @InjectRepository(Sentiment)
    private sentimentsRepository: Repository<Sentiment>,
  ) {}
  async create(createSentimentDto: CreateSentimentDto) {
    const newSentiment = new Sentiment();
    newSentiment.senti_label = createSentimentDto.senti_label;
    newSentiment.senti_score = createSentimentDto.senti_score;
    return this.sentimentsRepository.save(newSentiment);
  }

  findAll() {
    return this.sentimentsRepository.find();
  }

  async findOne(id: number) {
    const role = await this.sentimentsRepository.findOne({
      where: { senti_id: id },
    });
    if (!role) {
      throw new NotFoundException();
    }
    return this.sentimentsRepository.findOne({ where: { senti_id: id } });
  }

  async update(id: number, updateSentimentDto: UpdateSentimentDto) {
    const role = await this.sentimentsRepository.findOneBy({ senti_id: id });
    if (!role) {
      throw new NotFoundException();
    }
    const updatedRole = { ...role, ...updateSentimentDto };
    return this.sentimentsRepository.save(updatedRole);
  }

  async remove(id: number) {
    const role = await this.sentimentsRepository.findOneBy({ senti_id: id });
    if (!role) {
      throw new NotFoundException();
    }
    return this.sentimentsRepository.softRemove(role);
  }
}
