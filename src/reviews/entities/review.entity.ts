import { Book } from 'src/books/entities/book.entity';
import { ReportReview } from 'src/report_reviews/entities/report_review.entity';
import { Sentiment } from 'src/sentiments/entities/sentiment.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Review {
  @PrimaryGeneratedColumn()
  rev_id: number;

  @Column({})
  rev_star: number;

  @Column({})
  rev_comment: string;

  @CreateDateColumn()
  rev_date: Date;

  @OneToMany(() => ReportReview, (report_review) => report_review.review)
  reportReview: ReportReview[];

  @ManyToOne(() => User, (user) => user.review)
  user: User;

  @ManyToOne(() => Book, (book) => book.review)
  book: Book;

  @ManyToOne(() => Sentiment, (sentiment) => sentiment.review)
  sentiment: Sentiment;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
