import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  BadRequestException,
} from '@nestjs/common';
import { ReviewsService } from './reviews.service';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';

@Controller('reviews')
export class ReviewsController {
  constructor(private readonly reviewsService: ReviewsService) {}

  @Post()
  create(@Body() createReviewDto: CreateReviewDto) {
    console.log('createReviewDto:', createReviewDto);

    return this.reviewsService.create(createReviewDto);
  }

  @Get()
  findAll() {
    return this.reviewsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    const reviewId = parseInt(id, 10);
    if (isNaN(reviewId)) {
      throw new BadRequestException('Invalid review ID');
    }
    return this.reviewsService.findOne(reviewId);
  }

  @Get('book/:bookId') // เพิ่ม endpoint สำหรับดึงข้อมูลรีวิวตาม book_id
  findByBookId(@Param('bookId') bookId: string) {
    const parsedBookId = parseInt(bookId, 10);
    if (isNaN(parsedBookId)) {
      throw new BadRequestException('Invalid book ID');
    }
    return this.reviewsService.findByBookId(parsedBookId);
  }
  @Get('user/:userId')
  findByUserId(@Param('userId') userId: string) {
    const parsedUserId = parseInt(userId, 10);
    if (isNaN(parsedUserId)) {
      throw new BadRequestException('Invalid user ID');
    }
    return this.reviewsService.findByUserId(parsedUserId);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateReviewDto: UpdateReviewDto,
  ) {
    const reviewId = parseInt(id, 10);
    if (isNaN(reviewId)) {
      throw new BadRequestException('Invalid review ID');
    }
    return this.reviewsService.update(reviewId, updateReviewDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    const reviewId = parseInt(id, 10);
    if (isNaN(reviewId)) {
      throw new BadRequestException('Invalid review ID');
    }
    return this.reviewsService.remove(reviewId);
  }
}
