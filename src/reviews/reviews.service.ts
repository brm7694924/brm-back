import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { Review } from './entities/review.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Book } from 'src/books/entities/book.entity';
import { Sentiment } from 'src/sentiments/entities/sentiment.entity';
import { BooksService } from 'src/books/books.service';
import axios from 'axios';
import { ReviewSummary } from './entities/review.type';

@Injectable()
export class ReviewsService {
  private readonly apiUrl = 'http://localhost:8000'; // URL ของ FastAPI

  async predict(reviewText: string) {
    const response = await axios.post(`${this.apiUrl}/predict/`, {
      text: reviewText,
    });
    return response.data;
  }

  async predictTest(reviewText: string) {
    const response = await axios.post(`${this.apiUrl}/predict-test/`, {
      text: reviewText,
    });
    return response.data;
  }

  // async summarize(reviews_: ReviewSummary[]) {
  //   const response = await axios.post(`${this.apiUrl}/summarize/`, {
  //     // reviews: reviews_,
  //     reviews: reviews.map(review => ({ text: review.text })),
  //   });
  //   return response.data;
  // }

  // async summarize(reviews_: ReviewSummary[]) {
  //   console.log('Summarizing reviews:', { reviews: reviews_ });

  //   const response = await axios.post(`${this.apiUrl}/summarize/`, {
  //     reviews: [...reviews_],
  //   });
  //   return response.data;
  // }

  async summarize(reviews_: ReviewSummary[]) {
    console.log('Summarizing reviews:', { reviews: reviews_ });

    const response = await axios.post(`${this.apiUrl}/summarize/`, {
      reviews: reviews_.map((review) => ({ text: review.text })), // Correct format { text: "..." }
    });

    return response.data;
  }

  constructor(
    @InjectRepository(Review)
    private reviewsRepository: Repository<Review>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
    @InjectRepository(Sentiment)
    private sentimentsRepository: Repository<Sentiment>,
    private booksService: BooksService,
  ) {}

  async create(createReviewDto: CreateReviewDto) {
    try {
      const user = await this.usersRepository.findOne({
        where: { user_id: createReviewDto.user_id },
      });
      const book = await this.booksRepository.findOne({
        where: { book_id: createReviewDto.book_id },
      });

      console.log('createReviewDto:', createReviewDto);
      console.log('user:', user);
      console.log('book:', book);

      if (!user || !book) {
        throw new NotFoundException('User or Book not found');
      }

      // ตรวจสอบว่า user นี้เคยรีวิวหนังสือเล่มนี้หรือไม่
      const existingReview = await this.reviewsRepository.findOne({
        where: {
          user: { user_id: user.user_id },
          book: { book_id: book.book_id },
        },
      });

      if (existingReview) {
        return { message: 'User has already reviewed this book' };
      }

      // Call the predict function with rev_comment
      console.log(
        'Predicting sentiment for comment:',
        createReviewDto.rev_comment,
      );
      const prediction = await this.predict(createReviewDto.rev_comment);
      console.log('Prediction Result:', prediction);

      if (!prediction) {
        throw new BadRequestException('Invalid prediction result');
      }

      // Map sentiment score to senti_id
      let senti_id: number;
      if (prediction.sentiment_score === 1) {
        senti_id = 1;
      } else if (prediction.sentiment_score === 0) {
        senti_id = 2;
      } else if (prediction.sentiment_score === -1) {
        senti_id = 3;
      } else {
        throw new BadRequestException('Invalid sentiment score');
      }

      // Find the sentiment by senti_id
      const sentiment = await this.sentimentsRepository.findOne({
        where: { senti_id },
      });
      console.log('Sentiment:', sentiment);

      if (!sentiment) {
        throw new NotFoundException('Sentiment not found');
      }

      // Create a new review entry
      const newReview = new Review();
      newReview.rev_star = createReviewDto.rev_star;
      newReview.rev_comment = createReviewDto.rev_comment;
      newReview.user = user;
      newReview.book = book;
      newReview.sentiment = sentiment;

      const savedReview = await this.reviewsRepository.save(newReview);

      // Fetch all reviews for the book
      const allReviews = await this.reviewsRepository.find({
        where: { book: { book_id: book.book_id } },
      });
      const reviewComments: ReviewSummary[] = [];

      for (const review of allReviews) {
        reviewComments.push({
          text: review.rev_comment,
        });
      }

      // Summarize reviews
      const summary = await this.summarize(reviewComments);

      // Update book with pros and cons
      book.book_pros = summary.pros;
      book.book_cons = summary.cons;

      // Calculate average review star
      const totalStars = allReviews.reduce(
        (sum, review) => sum + review.rev_star,
        0,
      );
      const averageStars =
        allReviews.length > 0 ? totalStars / allReviews.length : 0; // ตรวจสอบว่ามีการรีวิวหรือไม่

      // Update book score
      book.book_score = isNaN(averageStars) ? 0 : averageStars; // ตรวจสอบอีกครั้งก่อนที่จะบันทึก

      console.log('Total Stars:', totalStars);
      console.log('Average Stars:', averageStars);

      if (!isNaN(book.book_score)) {
        await this.booksRepository.save(book); // บันทึกค่าเฉพาะเมื่อไม่ใช่ NaN
      } else {
        console.error('Invalid book score:', book.book_score);
      }

      return savedReview;
    } catch (error) {
      console.error('Error:', error);
      return { message: 'Error Creating Review' };
    }
  }

  findAll() {
    return this.reviewsRepository.find({
      relations: ['user', 'book', 'sentiment'],
    });
  }

  async findOne(id: number) {
    if (isNaN(id)) {
      throw new BadRequestException('Invalid review ID');
    }
    const review = await this.reviewsRepository.findOne({
      where: { rev_id: id },
    });
    if (!review) {
      throw new NotFoundException();
    }
    return this.reviewsRepository.findOne({
      where: { rev_id: id },
      relations: ['user', 'book', 'sentiment'],
    });
  }

  async findByBookId(bookId: number) {
    if (isNaN(bookId)) {
      throw new BadRequestException('Invalid book ID');
    }

    // ดึงข้อมูลรีวิวที่เกี่ยวข้องกับ book_id
    const reviews = await this.reviewsRepository.find({
      where: { book: { book_id: bookId } }, // ใช้การอ้างอิง book_id
      relations: ['user', 'book', 'sentiment'],
    });

    if (!reviews || reviews.length === 0) {
      throw new NotFoundException('No reviews found for this book');
    }

    return reviews;
  }

  async update(id: number, updateReviewDto: UpdateReviewDto) {
    if (isNaN(id)) {
      throw new BadRequestException('Invalid review ID');
    }
    const review = await this.reviewsRepository.findOne({
      where: { rev_id: id },
      relations: ['book', 'user', 'sentiment'],
    });
    if (!review) {
      throw new NotFoundException();
    }

    const user = await this.usersRepository.findOne({
      where: { user_id: updateReviewDto.user_id },
    });
    const book = await this.booksRepository.findOne({
      where: { book_id: updateReviewDto.book_id },
    });

    if (!user || !book) {
      throw new NotFoundException('User or Book not found');
    }

    // Call the predict function with rev_comment
    console.log(
      'Predicting sentiment for comment:',
      updateReviewDto.rev_comment,
    );
    const prediction = await this.predict(updateReviewDto.rev_comment);
    console.log('Prediction Result:', prediction);

    if (!prediction) {
      throw new BadRequestException('Invalid prediction result');
    }

    // Map sentiment score to senti_id
    let senti_id: number;
    if (prediction.sentiment_score === 1) {
      senti_id = 1;
    } else if (prediction.sentiment_score === 0) {
      senti_id = 2;
    } else if (prediction.sentiment_score === -1) {
      senti_id = 3;
    } else {
      throw new BadRequestException('Invalid sentiment score');
    }

    // Find the sentiment by senti_id
    const sentiment = await this.sentimentsRepository.findOne({
      where: { senti_id },
    });
    console.log('Sentiment:', sentiment);

    if (!sentiment) {
      throw new NotFoundException('Sentiment not found');
    }

    // Update the review entry
    review.rev_star = updateReviewDto.rev_star;
    review.rev_comment = updateReviewDto.rev_comment;
    review.user = user;
    review.book = book;
    review.sentiment = sentiment;

    const updatedReview = await this.reviewsRepository.save(review);

    // Fetch all reviews for the book
    const allReviews = await this.reviewsRepository.find({
      where: { book: { book_id: book.book_id } },
    });
    const reviewComments: ReviewSummary[] = [];

    for (const review of allReviews) {
      reviewComments.push({
        text: review.rev_comment,
      });
    }

    // Summarize reviews
    const summary = await this.summarize(reviewComments);

    // Update book with pros and cons
    book.book_pros = summary.pros;
    book.book_cons = summary.cons;

    // Calculate average review star
    const totalStars = allReviews.reduce(
      (sum, review) => sum + review.rev_star,
      0,
    );
    const averageStars =
      allReviews.length > 0 ? totalStars / allReviews.length : 0;

    // Update book score
    book.book_score = isNaN(averageStars) ? 0 : averageStars;

    if (!isNaN(book.book_score)) {
      await this.booksRepository.save(book);
    } else {
      console.error('Invalid book score:', book.book_score);
    }

    return updatedReview;
  }

  async remove(id: number) {
    if (isNaN(id)) {
      throw new BadRequestException('Invalid review ID');
    }
    const review = await this.reviewsRepository.findOne({
      where: { rev_id: id },
      relations: ['book'],
    });
    if (!review) {
      throw new NotFoundException();
    }

    const book = review.book;

    // ลบรีวิว
    await this.reviewsRepository.softRemove(review);

    // Fetch all remaining reviews for the book
    const allReviews = await this.reviewsRepository.find({
      where: { book: { book_id: book.book_id } },
    });

    // Calculate new average review star
    const totalStars = allReviews.reduce(
      (sum, review) => sum + review.rev_star,
      0,
    );
    const averageStars =
      allReviews.length > 0 ? totalStars / allReviews.length : 0;

    // Update book score
    book.book_score = averageStars;
    await this.booksRepository.save(book);

    return { message: 'Review deleted successfully' };
  }

  async findByUserId(userId: number) {
    if (isNaN(userId)) {
      throw new BadRequestException('Invalid user ID');
    }

    // ดึงข้อมูลรีวิวของผู้ใช้ตาม user_id โดยเรียงลำดับตามวันที่
    const reviews = await this.reviewsRepository
      .createQueryBuilder('review')
      .leftJoinAndSelect('review.user', 'user') // เชื่อมกับ user
      .leftJoinAndSelect('review.book', 'book') // เชื่อมกับ book
      .leftJoinAndSelect('review.sentiment', 'sentiment') // เชื่อมกับ sentiment
      .where('user.user_id = :userId', { userId })
      .orderBy('review.rev_date', 'DESC')
      .getMany();

    if (!reviews || reviews.length === 0) {
      throw new NotFoundException('No reviews found for this user');
    }

    // สร้างออบเจ็กต์ที่มีโครงสร้างที่ต้องการ
    const userReviewsResponse = {
      user: {
        user_id: reviews[0].user.user_id,
        user_name: reviews[0].user.user_name, // หรือชื่อผู้ใช้ของคุณ
      },
      reviews: reviews.map((review) => ({
        rev_id: review.rev_id,
        rev_star: review.rev_star,
        rev_comment: review.rev_comment,
        rev_date: review.rev_date,
        book: {
          book_id: review.book.book_id,
          title: review.book.book_title,
        },
        sentiment: review.sentiment,
      })),
    };

    return userReviewsResponse;
  }
}
