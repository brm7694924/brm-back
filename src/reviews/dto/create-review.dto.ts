import { IsNotEmpty, IsNumber } from 'class-validator';
export class CreateReviewDto {
  @IsNotEmpty()
  @IsNumber()
  rev_star: number;

  @IsNotEmpty()
  rev_comment: string;

  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  book_id: number;

  // @IsNotEmpty()
  // senti_id: number;
}
