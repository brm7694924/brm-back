import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReviewsService } from './reviews.service';
import { ReviewsController } from './reviews.controller';
import { Review } from './entities/review.entity';
import { User } from 'src/users/entities/user.entity';
import { Book } from 'src/books/entities/book.entity';
import { Sentiment } from 'src/sentiments/entities/sentiment.entity';
import { BooksModule } from 'src/books/books.module'; // นำเข้า BooksModule

@Module({
  imports: [
    TypeOrmModule.forFeature([Review, User, Book, Sentiment]),
    BooksModule, // เพิ่ม BooksModule ที่นี่
  ],
  controllers: [ReviewsController],
  providers: [ReviewsService],
})
export class ReviewsModule {}
