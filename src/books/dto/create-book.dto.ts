import { Transform } from 'class-transformer';
import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateBookDto {
  @IsNotEmpty()
  book_title: string;

  @IsNotEmpty()
  book_content: string;

  @IsNotEmpty()
  @IsNumber()
  @Transform(({ value }) => parseFloat(value), { toClassOnly: true })
  book_score: number;

  @IsNotEmpty()
  book_pros: string;

  @IsNotEmpty()
  book_cons: string;

  @IsNotEmpty()
  book_img: string = 'No-Image-Placeholder.jpg';

  @IsNotEmpty()
  myfavorite_id: number;
}
