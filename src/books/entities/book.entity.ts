import { Author } from 'src/authors/entities/author.entity';
import { BookAuthor } from 'src/book_author/entities/book_author.entity';
import { BookCategory } from 'src/book_category/entities/book_category.entity';
import { Category } from 'src/categories/entities/category.entity';
import { Myfavorite } from 'src/myfavorites/entities/myfavorite.entity';
import { Review } from 'src/reviews/entities/review.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
  OneToOne,
} from 'typeorm';

@Entity()
export class Book {
  @PrimaryGeneratedColumn()
  book_id: number;

  @Column()
  book_title: string;

  @Column()
  book_content: string;

  @Column('decimal', { precision: 3, scale: 1 })
  book_score: number;

  @Column('simple-array')
  book_pros: string[];

  @Column('simple-array')
  book_cons: string[];

  @Column({
    name: 'book_img',
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  book_img: string;

  @OneToMany(() => BookCategory, (book_category) => book_category.book)
  bookCategory: [];

  @OneToMany(() => BookAuthor, (book_author) => book_author.book)
  bookAuthor: [];

  @ManyToOne(() => Myfavorite, (myfavorite) => myfavorite.book)
  myfavorite: Myfavorite[];

  // @OneToOne(() => Myfavorite, (myfavorite) => myfavorite.book)
  // myfavorite: Myfavorite;

  @OneToMany(() => Review, (review) => review.book)
  review: Review[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
