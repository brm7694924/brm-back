import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles,
  UploadedFile,
  Res,
} from '@nestjs/common';
import { BooksService } from './books.service';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import {
  FileFieldsInterceptor,
  FileInterceptor,
} from '@nestjs/platform-express';
import { extname } from 'path';
import { Response } from 'express';
import { Book } from './entities/book.entity';
import { unlinkSync, existsSync, readdirSync } from 'fs'; // ใช้สำหรับการลบไฟล์เก่า

function getNextFileName(): string {
  const directoryPath = './public/images';
  const files = readdirSync(directoryPath);
  const numbers = files
    .map((file) => parseInt(file.replace('_BookImg' + extname(file), ''), 10))
    .filter((num) => !isNaN(num))
    .sort((a, b) => a - b);

  const nextNumber = numbers.length > 0 ? numbers[numbers.length - 1] + 1 : 1;
  return nextNumber.toString().padStart(4, '0');
}

@Controller('books')
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @Post()
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'book_img', maxCount: 1 }], {
      storage: diskStorage({
        destination: './public/images',
        filename: (req, file, cb) => {
          const uniqueName =
            getNextFileName() + '_BookImg' + extname(file.originalname);
          cb(null, uniqueName);
        },
      }),
    }),
  )
  async create(
    @Body() createBookDto: CreateBookDto,
    @UploadedFiles() files: Record<string, Express.Multer.File[]>,
  ): Promise<Book> {
    console.log('Files received:', files); // Check the files object
    console.log('Book Image:', files['book_img']); // Check the book_img key in files

    // Check if 'book_img' exists in files and if the array is not empty
    if (!files || !files['book_img'] || files['book_img'].length === 0) {
      throw new Error('No book image file uploaded');
    }

    createBookDto.book_img = files['book_img'][0].filename;
    const book = await this.booksService.create(createBookDto);
    await this.booksService.updateProsCons(book.book_id, [], []); // ตรวจสอบการเรียกใช้ฟังก์ชัน updateProsCons
    return book;
  }

  @Get('all')
  findAll() {
    return this.booksService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.booksService.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const book = await this.booksService.findOne(+id);
    res.sendFile(book.book_img, { root: './public/images' });
  }

  @Get()
  findRank() {
    return this.booksService.findRank();
  }

  @Get('category/rank/:id')
  findRankByCategory(@Param('id') id: string) {
    return this.booksService.findRankByCategory(+id);
  }

  @Get('author/rank/:id')
  findRankByAuthor(@Param('id') id: string) {
    return this.booksService.findRankByAuthor(+id);
  }

  @Patch(':id')
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'book_img', maxCount: 1 }], {
      storage: diskStorage({
        destination: './public/images',
        filename: (req, file, cb) => {
          const uniqueName =
            getNextFileName() + '_BookImg' + extname(file.originalname);
          cb(null, uniqueName);
        },
      }),
    }),
  )
  async update(
    @Param('id') id: string,
    @Body() updateBookDto: UpdateBookDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    // ดึงข้อมูลหนังสือก่อนเพื่อให้ได้ชื่อไฟล์รูปภาพเก่า
    const book = await this.booksService.findOne(+id);

    // เก็บชื่อไฟล์รูปภาพเก่า
    const oldFilePath = `./public/images/${book.book_img}`;

    // หากมีการอัปโหลดไฟล์ใหม่
    if (file) {
      // กำหนดชื่อไฟล์รูปภาพใหม่ให้กับ DTO
      updateBookDto.book_img = file.filename;

      // ตรวจสอบว่ามีไฟล์รูปภาพเก่าอยู่หรือไม่และทำการลบ
      if (existsSync(oldFilePath)) {
        unlinkSync(oldFilePath); // ลบไฟล์รูปภาพเก่า
      }
    }

    // ทำการอัปเดตข้อมูลหนังสือในฐานข้อมูล
    return await this.booksService.update(+id, updateBookDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.booksService.remove(+id);
  }

  @Get('category/:id')
  async getBooksByCategory(@Param('id') id: string) {
    return this.booksService.findBooksByCategory(+id);
  }

  @Get('author/:id')
  async getBooksByAuthor(@Param('id') id: string) {
    return this.booksService.findBooksByAuthor(+id);
  }
}
