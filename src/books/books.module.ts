import { Module } from '@nestjs/common';
import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from './entities/book.entity';
import { Myfavorite } from 'src/myfavorites/entities/myfavorite.entity';
import { MyfavoritesModule } from 'src/myfavorites/myfavorites.module';
import { Review } from 'src/reviews/entities/review.entity';

@Module({
  imports: [
    MyfavoritesModule,
    TypeOrmModule.forFeature([Book, Myfavorite, Review]),
  ],
  controllers: [BooksController],
  providers: [BooksService],
  exports: [BooksService],
})
export class BooksModule {}
