import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from './entities/book.entity';
import { Repository } from 'typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { Myfavorite } from 'src/myfavorites/entities/myfavorite.entity';
import axios from 'axios';
import { existsSync, promises as fs } from 'fs';
import * as path from 'path';
import { v4 as uuidv4 } from 'uuid';
import { Author } from 'src/authors/entities/author.entity';
import { Review } from 'src/reviews/entities/review.entity';

interface ScoreAccumulator {
  [book_id: number]: {
    total: number;
    count: number;
  };
}

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
    @InjectRepository(Myfavorite)
    private myfavoritesRepository: Repository<Myfavorite>,
    @InjectRepository(Review)
    private reviewsRepository: Repository<Review>,
  ) {}
  async create(createBookDto: CreateBookDto) {
    const myfavorite = await this.myfavoritesRepository.findOne({
      where: { myfav_id: createBookDto.myfavorite_id },
    });
    const newBook = new Book();
    newBook.book_title = createBookDto.book_title;
    newBook.book_content = createBookDto.book_content;
    newBook.book_score = createBookDto.book_score;
    newBook.book_pros = Array.isArray(createBookDto.book_pros)
      ? createBookDto.book_pros
      : [createBookDto.book_pros];
    newBook.book_cons = Array.isArray(createBookDto.book_cons)
      ? createBookDto.book_cons
      : [createBookDto.book_cons];
    newBook.book_img = createBookDto.book_img;
    newBook.myfavorite = [myfavorite];
    return this.booksRepository.save(newBook);
  }

  async updateProsCons(bookId: number, pros: string[], cons: string[]) {
    const book = await this.booksRepository.findOne({
      where: { book_id: bookId },
    });
    if (!book) {
      throw new NotFoundException();
    }
    book.book_pros = pros;
    book.book_cons = cons;
    return this.booksRepository.save(book);
  }

  private countOccurrences(
    reviews: Review[],
    sentiId: number,
  ): Record<string, number> {
    return reviews.reduce(
      (acc, review) => {
        console.log('Review Sentiment:', review.sentiment); // เพิ่มการพิมพ์ผลลัพธ์เพื่อตรวจสอบ
        if (review.sentiment && review.sentiment.senti_id === sentiId) {
          review.rev_comment.split(', ').forEach((comment) => {
            acc[comment] = (acc[comment] || 0) + 1;
          });
        }
        return acc;
      },
      {} as Record<string, number>,
    );
  }

  findAll() {
    return this.booksRepository.find({ relations: ['myfavorite'] });
  }

  async findRank() {
    const books = await this.booksRepository
      .createQueryBuilder('book')
      .select([
        'book.book_id',
        'book.book_title',
        'book.book_content',
        'book.book_score',
      ])
      .where('book.book_score IS NOT NULL')
      .andWhere('book.book_score != ""') // ป้องกันค่าที่เป็น string ว่าง
      .andWhere('book.book_score >= 0') // กรองค่าที่ไม่เป็น NaN
      .orderBy('book.book_score', 'DESC')
      .addOrderBy('book.book_title', 'ASC')
      .limit(10)
      .getMany();

    const booksWithRank = books.map((book, index) => ({
      rank: index + 1,
      book_id: book.book_id,
      book_title: book.book_title,
      book_content: book.book_content,
      book_score: book.book_score,
    }));

    return booksWithRank;
  }

  async findRankByCategory(categoryId: number) {
    const books = await this.booksRepository
      .createQueryBuilder('book')
      .leftJoin('book.bookCategory', 'bookCategory')
      .leftJoin('bookCategory.category', 'category')
      .select([
        'book.book_id',
        'book.book_title',
        'book.book_content',
        'book.book_score',
      ])
      .where('book.book_score IS NOT NULL')
      .andWhere('book.book_score != ""') // ป้องกันค่าที่เป็น string ว่าง
      .andWhere('book.book_score >= 0') // กรองค่าที่ไม่เป็น NaN
      .andWhere('category.category_id = :categoryId', { categoryId }) // กรองตาม category_id
      .orderBy('book.book_score', 'DESC')
      .addOrderBy('book.book_title', 'ASC')
      .limit(10)
      .getMany();

    console.log('Books found:', books.length);

    const booksWithRank = books.map((book, index) => ({
      rank: index + 1,
      book_id: book.book_id,
      book_title: book.book_title,
      book_content: book.book_content,
      book_score: book.book_score,
    }));

    return booksWithRank;
  }

  async findRankByAuthor(authorId: number) {
    const books = await this.booksRepository
      .createQueryBuilder('book')
      .leftJoin('book.bookAuthor', 'bookAuthor')
      .leftJoin('bookAuthor.author', 'author')
      .select([
        'book.book_id',
        'book.book_title',
        'book.book_content',
        'book.book_score',
      ])
      .where('book.book_score IS NOT NULL')
      .andWhere('book.book_score != ""') // ป้องกันค่าที่เป็น string ว่าง
      .andWhere('book.book_score >= 0') // กรองค่าที่ไม่เป็น NaN
      .andWhere('author.author_id = :authorId', { authorId }) // กรองตาม author_id
      .orderBy('book.book_score', 'DESC')
      .addOrderBy('book.book_title', 'ASC')
      .limit(10)
      .getMany();

    console.log('Books found:', books.length);

    const booksWithRank = books.map((book, index) => ({
      rank: index + 1,
      book_id: book.book_id,
      book_title: book.book_title,
      book_content: book.book_content,
      book_score: book.book_score,
    }));

    return booksWithRank;
  }

  findOne(id: number) {
    const book = this.booksRepository.findOne({
      where: { book_id: id },
      relations: ['myfavorite'],
    });
    if (!book) {
      throw new NotFoundException();
    }
    return this.booksRepository.findOne({ where: { book_id: id } });
  }

  async update(id: number, updateBookDto: UpdateBookDto) {
    const book = await this.booksRepository.findOneBy({ book_id: id });
    if (!book) {
      throw new NotFoundException();
    }
    const updatedBook = {
      ...book,
      ...updateBookDto,
      book_pros: Array.isArray(updateBookDto.book_pros)
        ? updateBookDto.book_pros
        : [updateBookDto.book_pros],
      book_cons: Array.isArray(updateBookDto.book_cons)
        ? updateBookDto.book_cons
        : [updateBookDto.book_cons],
    };
    return this.booksRepository.save(updatedBook);
  }

  async remove(id: number) {
    const book = await this.booksRepository.findOneBy({ book_id: id });
    if (!book) {
      throw new NotFoundException();
    }
    return this.booksRepository.softRemove(book);
  }

  async findBooksByCategory(categoryId: number): Promise<any> {
    const books = await this.booksRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.bookCategory', 'bookCategory')
      .innerJoinAndSelect('bookCategory.category', 'category')
      .where('category.category_id = :id', { id: categoryId })
      .getMany();

    if (!books.length) {
      throw new NotFoundException(
        `No books found for category ID ${categoryId}`,
      );
    }

    // สร้างโครงสร้างข้อมูลที่ต้องการ
    const categoryBooks = books.map((book) => {
      const { bookCategory, ...bookDetails } = book; // แยก bookCategory ออกไป
      const categories = (bookCategory as { category: Category }[]).map(
        (bc) => bc.category,
      ); // ข้อมูลหมวดหมู่ทั้งหมดของหนังสือ

      return {
        ...bookDetails,
        categories: categories.map((category) => ({
          category_id: category.category_id,
          category_name: category.category_name,
        })),
      };
    });

    // ใช้ Set เพื่อลดข้อมูลหมวดหมู่ที่ซ้ำกัน
    const categories = categoryBooks.flatMap((b) => b.categories);
    const uniqueCategories = Array.from(
      new Set(categories.map((c) => c.category_id)),
    ).map((category_id) => ({
      category_id,
      category_name: categories.find((c) => c.category_id === category_id)
        ?.category_name,
    }));

    // สร้างข้อมูลที่จัดกลุ่มตามหมวดหมู่
    const groupedResult = uniqueCategories.map((category) => {
      return {
        category_id: category.category_id,
        category_name: category.category_name,
        books: categoryBooks
          .filter((book) =>
            book.categories.some(
              (cat) => cat.category_id === category.category_id,
            ),
          )
          .map((b) => ({
            book_id: b.book_id,
            book_title: b.book_title,
          })),
      };
    });

    return groupedResult;
  }

  async findBooksByAuthor(authorId: number): Promise<any> {
    const books = await this.booksRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.bookAuthor', 'bookAuthor')
      .innerJoinAndSelect('bookAuthor.author', 'author')
      .where('author.author_id = :id', { id: authorId })
      .getMany();

    console.log('Books found:', books); // เพิ่มบรรทัดนี้เพื่อตรวจสอบผลลัพธ์

    if (!books.length) {
      throw new NotFoundException(`No books found for author ID ${authorId}`);
    }

    // สร้างโครงสร้างข้อมูลที่ต้องการ
    const authorBooks = books.map((book) => {
      const { bookAuthor = [], ...bookDetails } = book; // แยก bookAuthor ออกไปและให้ค่าเริ่มต้นเป็น array
      const authors = (bookAuthor as { author: Author }[]).map(
        (ba) => ba.author,
      ); // ข้อมูลผู้เขียนทั้งหมดของหนังสือ

      return {
        ...bookDetails,
        authors: authors.map((author) => ({
          author_id: author.author_id,
          author_name: author.author_name,
        })),
      };
    });

    // ใช้ Set เพื่อลดข้อมูลผู้เขียนที่ซ้ำกัน
    const allAuthors = authorBooks.flatMap((b) => b.authors);
    const uniqueAuthors = Array.from(
      new Set(allAuthors.map((a) => a.author_id)),
    ).map((author_id) => ({
      author_id,
      author_name: allAuthors.find((a) => a.author_id === author_id)
        ?.author_name,
    }));

    // สร้างข้อมูลที่จัดกลุ่มตามผู้เขียน
    const groupedResult = uniqueAuthors.map((author) => {
      return {
        author_id: author.author_id,
        author_name: author.author_name,
        books: authorBooks // ใช้ authorBooks
          .filter((book) =>
            book.authors.some((a) => a.author_id === author.author_id),
          )
          .map((b) => ({
            book_id: b.book_id,
            book_title: b.book_title,
          })),
      };
    });

    return groupedResult;
  }

  async getRank(): Promise<Book[]> {
    const books = await this.booksRepository
      .createQueryBuilder('book')
      .where('book.book_score IS NOT NULL')
      .andWhere('book.book_score != ""') // ป้องกันค่าที่เป็น string ว่าง
      .andWhere('book.book_score >= 0') // กรองค่าที่ไม่เป็น NaN
      .orderBy('book.book_score', 'DESC')
      .addOrderBy('book.book_title', 'ASC')
      .limit(10)
      .getMany();

    return books;
  }
}
