import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { Role } from 'src/roles/entities/role.entity';
import * as bcrypt from 'bcrypt';
import { join } from 'path';
import * as jwt from 'jsonwebtoken';
import * as dotenv from 'dotenv';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
  ) {}

  private readonly secretKey = process.env.JWT_SECRET_KEY; // ดึงคีย์ลับจาก environment variables

  async create(createUserDto: CreateUserDto) {
    const role = await this.rolesRepository.findOne({
      where: { role_id: createUserDto.role_id },
    });

    if (!role) {
      throw new NotFoundException('Role not found'); // ป้องกันหากไม่พบ role
    }

    const newUser = new User();
    const salt = await bcrypt.genSalt(); // สร้าง salt
    const hash = await bcrypt.hash(createUserDto.user_password, salt); // hash รหัสผ่าน
    newUser.user_password = hash;
    newUser.user_email = createUserDto.user_email;
    newUser.user_name = createUserDto.user_name;
    newUser.user_img = createUserDto.user_img || join('default.jpg'); // ใช้ค่าพื้นฐานถ้าไม่มีรูป
    newUser.role = role; // กำหนด role

    return this.usersRepository.save(newUser);
  }

  async findAll() {
    return this.usersRepository.find({ relations: ['role'] });
  }

  async findOne(id: number) {
    const user = await this.usersRepository.findOne({
      where: { user_id: id },
      relations: ['role'],
    });
    if (!user) {
      throw new NotFoundException();
    }
    return user; // ส่งคืน user ที่พบ
  }

  async findOneByEmail(email: string) {
    const user = await this.usersRepository.findOne({
      where: { user_email: email },
    });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.findOneBy({ user_id: id });
    if (!user) {
      throw new NotFoundException();
    }
    const updatedUser = { ...user, ...updateUserDto };
    return this.usersRepository.save(updatedUser);
  }

  async remove(id: number) {
    const user = await this.usersRepository.findOneBy({ user_id: id });
    if (!user) {
      throw new NotFoundException();
    }
    return this.usersRepository.softRemove(user);
  }

  async login(createUserDto: CreateUserDto) {
    console.log('Attempting login with:', createUserDto);
    const user = await this.usersRepository.findOne({
      where: { user_email: createUserDto.user_email },
    });

    if (!user) {
      console.log('User not found');
      throw new NotFoundException('User not found');
    }

    const isMatch = await bcrypt.compare(
      createUserDto.user_password,
      user.user_password,
    );

    if (!isMatch) {
      console.log('Password does not match');
      throw new UnauthorizedException('Incorrect password');
    }

    console.log('Login successful for user:', user);
    return {
      user: {
        user_id: user.user_id,
        user_email: user.user_email,
        user_name: user.user_name,
        user_img: user.user_img,
      },
      message: 'Login successful',
    };
  }

  async compareHash(plainText: string, hash: string) {
    try {
      const isMatch = await bcrypt.compare(plainText, hash);
      if (!isMatch) {
        throw new UnauthorizedException('Invalid hash');
      }
      return { message: 'Hash matches' };
    } catch (error) {
      throw new UnauthorizedException('Invalid hash');
    }
  }
}
