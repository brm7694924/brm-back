import { IsNotEmpty, IsOptional, Matches, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  user_email: string;

  @IsNotEmpty()
  @MinLength(4)
  user_name: string;

  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  )
  user_password: string;

  @IsOptional()
  user_img: string = 'public/images/default_UserImg.jpg';

  @IsNotEmpty()
  role_id: number;
}
