import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
  UploadedFiles,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {
  FileFieldsInterceptor,
  FileInterceptor,
} from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

// @UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'user_img', maxCount: 1 }], {
      storage: diskStorage({
        destination: './public/images', // Updated to store in public/images
        filename: (req, file, cb) => {
          const uniqueName =
            file.originalname.replace(/\.[^/.]+$/, '') +
            '_UserImg' + // Suffix UserImg
            extname(file.originalname);
          cb(null, uniqueName);
        },
      }),
    }),
  )
  async create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFiles() files: Record<string, Express.Multer.File[]>,
  ) {
    // Check if the user_img field exists and contains a file
    if (!files['user_img'] || files['user_img'].length === 0) {
      throw new BadRequestException('File is required');
    }
    createUserDto.user_img = files['user_img'][0].filename; // Save the file name
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Get(':email')
  findOneByEmail(@Param('email') email: string) {
    return this.usersService.findOneByEmail(email);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const user = await this.usersService.findOne(+id);
    res.sendFile(user.user_img, { root: './public/images' }); // Updated path
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('user_img', {
      storage: diskStorage({
        destination: './public/images', // Updated to store in public/images
        filename: (req, file, cb) => {
          const uniqueName =
            uuidv4() +
            '_UserImg' + // Suffix UserImg
            extname(file.originalname);
          return cb(null, uniqueName);
        },
      }),
    }),
  )
  async update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateUserDto.user_img = file.filename; // Save the file name
    }
    return await this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }

  @Post('compare-hash')
  compareHash(
    @Body('plainText') plainText: string,
    @Body('hash') hash: string,
  ) {
    return this.usersService.compareHash(plainText, hash);
  }
}
