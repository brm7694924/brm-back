import { FeedComment } from 'src/feed_comments/entities/feed_comment.entity';
import { Feed } from 'src/feeds/entities/feed.entity';
import { Myfavorite } from 'src/myfavorites/entities/myfavorite.entity';
import { Review } from 'src/reviews/entities/review.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  user_id: number;

  @Column({ unique: true })
  user_email: string;

  @Column({})
  user_name: string;

  @Column({ unique: true })
  user_password: string;

  @Column({
    name: 'user_img',
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  user_img: string;

  @CreateDateColumn()
  user_start_date: Date;

  @ManyToOne(() => Role, (role) => role.user)
  role: Role;

  @OneToMany(() => FeedComment, (feed_comment) => feed_comment.user)
  feedComment: FeedComment[];

  @OneToMany(() => Myfavorite, (myfavorite) => myfavorite.user)
  myfavorite: Myfavorite[];

  @OneToMany(() => Feed, (feed) => feed.user)
  feed: Feed[];

  @OneToMany(() => Review, (review) => review.user)
  review: Review[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
