import { Module } from '@nestjs/common';
import { FeedsService } from './feeds.service';
import { FeedsController } from './feeds.controller';
import { Feed } from './entities/feed.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Feed, User])],
  controllers: [FeedsController],
  providers: [FeedsService],
  exports: [FeedsModule],
})
export class FeedsModule {}
