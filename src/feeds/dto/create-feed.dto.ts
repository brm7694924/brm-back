import { Transform } from 'class-transformer';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { User } from 'src/users/entities/user.entity';
export class CreateFeedDto {
  @IsNotEmpty()
  feed_img = 'No-Image-Placeholder.jpg';

  @IsNotEmpty()
  feed_title: string;

  @IsNotEmpty()
  feed_text: string;

  @IsNotEmpty()
  @IsNumber()
  @Transform(({ value }) => parseFloat(value), { toClassOnly: true })
  feed_like: number;

  @IsNotEmpty()
  @IsNumber()
  @Transform(({ value }) => parseFloat(value), { toClassOnly: true })
  feed_dislike: number;

  @IsNotEmpty()
  user_id: number;
}
