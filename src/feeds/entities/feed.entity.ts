import { FeedComment } from 'src/feed_comments/entities/feed_comment.entity';
import { ReportFeed } from 'src/report_feeds/entities/report_feed.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Feed {
  @PrimaryGeneratedColumn()
  feed_id: number;

  @Column({
    name: 'feed_img',
    length: '128',
    default: 'No-Image-Placeholder.jpg',
  })
  feed_img: string;

  @Column({})
  feed_title: string;

  @Column({})
  feed_text: string;

  @Column({})
  feed_like: number;

  @Column({})
  feed_dislike: number;

  @CreateDateColumn()
  feed_date: Date;

  @OneToMany(() => FeedComment, (feed_comment) => feed_comment.feed)
  feedComment: FeedComment[];

  @OneToMany(() => ReportFeed, (report_feed) => report_feed.feed)
  reportFeed: ReportFeed[];

  @ManyToOne(() => User, (user) => user.feed)
  user: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
