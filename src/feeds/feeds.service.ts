import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateFeedDto } from './dto/create-feed.dto';
import { UpdateFeedDto } from './dto/update-feed.dto';
import { Feed } from './entities/feed.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class FeedsService {
  constructor(
    @InjectRepository(Feed)
    private feedsRepository: Repository<Feed>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}
  async create(createFeedDto: CreateFeedDto) {
    const user = await this.usersRepository.findOne({
      where: { user_id: createFeedDto.user_id },
    });
    const newFeed = new Feed();
    newFeed.feed_img = createFeedDto.feed_img;
    newFeed.feed_title = createFeedDto.feed_title;
    newFeed.feed_text = createFeedDto.feed_text;
    newFeed.feed_like = createFeedDto.feed_like;
    newFeed.feed_dislike = createFeedDto.feed_dislike;
    newFeed.user = user;
    return this.feedsRepository.save(newFeed);
  }

  findAll() {
    return this.feedsRepository.find({});
  }

  findOne(id: number) {
    const feed = this.feedsRepository.findOne({
      where: { feed_id: id },
    });
    if (!feed) {
      throw new NotFoundException();
    }
    return this.feedsRepository.findOne({ where: { feed_id: id } });
  }

  async findByUserId(userId: number) {
    if (isNaN(userId)) {
      throw new BadRequestException('Invalid user ID');
    }

    // ดึงข้อมูลรีวิวของผู้ใช้ตาม user_id โดยเรียงลำดับตามวันที่
    const feeds = await this.feedsRepository
      .createQueryBuilder('feed')
      .leftJoinAndSelect('feed.user', 'user') // เชื่อมกับ user
      .where('user.user_id = :userId', { userId })
      .orderBy('feed.feed_date', 'DESC')
      .getMany();

    if (!feeds || feeds.length === 0) {
      throw new NotFoundException('No feeds found for this user');
    }

    // สร้างออบเจ็กต์ที่มีโครงสร้างที่ต้องการ
    const userFeedsResponse = {
      user: {
        user_id: feeds[0].user.user_id,
        user_name: feeds[0].user.user_name, // หรือชื่อผู้ใช้ของคุณ
      },
      feed: feeds.map((feed) => ({
        feed_id: feed.feed_id,
        feed_title: feed.feed_title,
        feed_text: feed.feed_text,
        feed_date: feed.feed_date,
      })),
    };

    return userFeedsResponse;
  }

  async update(id: number, updateFeedDto: UpdateFeedDto) {
    const feed = await this.feedsRepository.findOneBy({ feed_id: id });
    if (!feed) {
      throw new NotFoundException();
    }
    const updatedFeed = { ...feed, ...updateFeedDto };
    return this.feedsRepository.save(updatedFeed);
  }

  async remove(id: number) {
    const feed = await this.feedsRepository.findOneBy({ feed_id: id });
    if (!feed) {
      throw new NotFoundException();
    }
    return this.feedsRepository.softRemove(feed);
  }
}
