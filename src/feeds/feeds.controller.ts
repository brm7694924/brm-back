import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UploadedFiles,
  UseInterceptors,
  Res,
  UploadedFile,
  BadRequestException,
} from '@nestjs/common';
import { FeedsService } from './feeds.service';
import { CreateFeedDto } from './dto/create-feed.dto';
import { UpdateFeedDto } from './dto/update-feed.dto';
import {
  FileFieldsInterceptor,
  FileInterceptor,
} from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { Response } from 'express';

@Controller('feeds')
export class FeedsController {
  constructor(private readonly feedsService: FeedsService) {}

  @Post()
  @UseInterceptors(
    FileFieldsInterceptor([{ name: 'feed_img', maxCount: 1 }], {
      storage: diskStorage({
        destination: './public/images', // Updated to store in public/images
        filename: (req, file, cb) => {
          const uniqueName =
            file.originalname.replace(/\.[^/.]+$/, '') +
            '_FeedImg' + // Suffix FeedImg
            extname(file.originalname);
          cb(null, uniqueName);
        },
      }),
    }),
  )
  create(
    @Body() createFeedDto: CreateFeedDto,
    @UploadedFiles() files: Record<string, Express.Multer.File[]>,
  ) {
    createFeedDto.feed_img = files['feed_img'][0].filename;
    return this.feedsService.create(createFeedDto);
  }

  @Get()
  findAll() {
    return this.feedsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.feedsService.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const feed = await this.feedsService.findOne(+id);
    res.sendFile(feed.feed_img, { root: './public/images' }); // Updated path
  }

  @Get('user/:userId')
  findByUserId(@Param('userId') userId: string) {
    const parsedUserId = parseInt(userId, 10);
    if (isNaN(parsedUserId)) {
      throw new BadRequestException('Invalid user ID');
    }
    return this.feedsService.findByUserId(parsedUserId);
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('feed_img', {
      storage: diskStorage({
        destination: './public/images', // Updated to store in public/images
        filename: (req, file, cb) => {
          const uniqueName =
            uuidv4() +
            '_FeedImg' + // Suffix FeedImg
            extname(file.originalname);
          return cb(null, uniqueName);
        },
      }),
    }),
  )
  async update(
    @Param('id') id: string,
    @Body() updateFeedDto: UpdateFeedDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateFeedDto.feed_img = file.filename; // Save the file name
    }
    return await this.feedsService.update(+id, updateFeedDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.feedsService.remove(+id);
  }
}
