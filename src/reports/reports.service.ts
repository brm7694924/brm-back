import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Report } from './entities/report.entity';

@Injectable()
export class ReportsService {
  constructor(
    @InjectRepository(Report)
    private reportsRepository: Repository<Report>,
  ) {}
  create(createReportDto: CreateReportDto) {
    const newReport = new Report();
    newReport.rep_name = createReportDto.rep_name;
    newReport.rep_user = createReportDto.rep_user;
    return this.reportsRepository.save(newReport);
  }

  findAll() {
    return this.reportsRepository.find({});
  }

  findOne(id: number) {
    const report = this.reportsRepository.findOne({
      where: { rep_id: id },
    });
    if (!report) {
      throw new NotFoundException();
    }
    return this.reportsRepository.findOne({ where: { rep_id: id } });
  }

  async update(id: number, updateReportDto: UpdateReportDto) {
    const report = await this.reportsRepository.findOneBy({ rep_id: id });
    if (!report) {
      throw new NotFoundException();
    }
    const updatedReport = { ...report, ...updateReportDto };
    return this.reportsRepository.save(updatedReport);
  }

  async remove(id: number) {
    const report = await this.reportsRepository.findOneBy({ rep_id: id });
    if (!report) {
      throw new NotFoundException();
    }
    return this.reportsRepository.softRemove(report);
  }
}
