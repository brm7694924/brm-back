import { IsNotEmpty } from 'class-validator';
export class CreateReportDto {
  @IsNotEmpty()
  rep_name: string;

  @IsNotEmpty()
  rep_user: string;
}
