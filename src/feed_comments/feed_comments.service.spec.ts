import { Test, TestingModule } from '@nestjs/testing';
import { FeedCommentsService } from './feed_comments.service';

describe('FeedCommentsService', () => {
  let service: FeedCommentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FeedCommentsService],
    }).compile();

    service = module.get<FeedCommentsService>(FeedCommentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
