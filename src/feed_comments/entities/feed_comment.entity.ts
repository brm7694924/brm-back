import { Feed } from 'src/feeds/entities/feed.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class FeedComment {
  @PrimaryGeneratedColumn()
  feed_com_id: number;

  @Column({})
  feed_com_text: string;

  @ManyToOne(() => Feed, (feed) => feed.feedComment)
  feed: Feed;

  @ManyToOne(() => User, (user) => user.feedComment)
  user: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
