import { IsNotEmpty } from 'class-validator';

export class CreateFeedCommentDto {
  @IsNotEmpty()
  feed_com_text: string;

  @IsNotEmpty()
  feed_id: number;

  @IsNotEmpty()
  user_id: number;
}
