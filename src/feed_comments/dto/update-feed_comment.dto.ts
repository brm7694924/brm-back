import { PartialType } from '@nestjs/mapped-types';
import { CreateFeedCommentDto } from './create-feed_comment.dto';

export class UpdateFeedCommentDto extends PartialType(CreateFeedCommentDto) {}
