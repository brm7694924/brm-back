import { Test, TestingModule } from '@nestjs/testing';
import { FeedCommentsController } from './feed_comments.controller';
import { FeedCommentsService } from './feed_comments.service';

describe('FeedCommentsController', () => {
  let controller: FeedCommentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FeedCommentsController],
      providers: [FeedCommentsService],
    }).compile();

    controller = module.get<FeedCommentsController>(FeedCommentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
