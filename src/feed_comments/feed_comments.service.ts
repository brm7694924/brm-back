import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateFeedCommentDto } from './dto/create-feed_comment.dto';
import { UpdateFeedCommentDto } from './dto/update-feed_comment.dto';
import { FeedComment } from './entities/feed_comment.entity';
import { Feed } from 'src/feeds/entities/feed.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class FeedCommentsService {
  constructor(
    @InjectRepository(FeedComment)
    private feedCommentsRepository: Repository<FeedComment>,
    @InjectRepository(Feed)
    private feedsRepository: Repository<Feed>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}
  async create(createFeedCommentDto: CreateFeedCommentDto) {
    const feed = await this.feedsRepository.findOne({
      where: { feed_id: createFeedCommentDto.feed_id },
    });
    const user = await this.usersRepository.findOne({
      where: { user_id: createFeedCommentDto.user_id },
    });
    const newFeedComment = new FeedComment();
    newFeedComment.feed_com_text = createFeedCommentDto.feed_com_text;
    newFeedComment.feed = feed;
    newFeedComment.user = user;
    return this.feedCommentsRepository.save(newFeedComment);
  }

  findByFeed(feedId: number) {
    return this.feedCommentsRepository.find({
      where: { feed: { feed_id: feedId } },
      relations: ['feed'],
    });
  }

  findByUser(userId: number) {
    return this.feedCommentsRepository.find({
      where: { user: { user_id: userId } },
      relations: ['user'],
    });
  }

  findAll() {
    return this.feedCommentsRepository.find({ relations: ['feed', 'user'] });
  }

  findOne(id: number) {
    const feedComment = this.feedCommentsRepository.findOne({
      where: { feed_com_id: id },
      relations: ['feed', 'user'],
    });
    if (!feedComment) {
      throw new NotFoundException();
    }
    return this.feedCommentsRepository.findOne({ where: { feed_com_id: id } });
  }

  async update(id: number, updateFeedCommentDto: UpdateFeedCommentDto) {
    const feedComment = await this.feedCommentsRepository.findOneBy({
      feed_com_id: id,
    });
    if (!feedComment) {
      throw new NotFoundException();
    }
    const updatedFeedComment = { ...feedComment, ...updateFeedCommentDto };
    return this.feedCommentsRepository.save(updatedFeedComment);
  }

  async remove(id: number) {
    const feedComment = await this.feedCommentsRepository.findOneBy({
      feed_com_id: id,
    });
    if (!feedComment) {
      throw new NotFoundException();
    }
    return this.feedCommentsRepository.softRemove(feedComment);
  }
}
