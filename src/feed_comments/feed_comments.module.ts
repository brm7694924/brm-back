import { Module } from '@nestjs/common';
import { FeedCommentsService } from './feed_comments.service';
import { FeedCommentsController } from './feed_comments.controller';
import { FeedComment } from './entities/feed_comment.entity';
import { Feed } from 'src/feeds/entities/feed.entity';
import { User } from 'src/users/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeedsModule } from 'src/feeds/feeds.module';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    FeedsModule,
    UsersModule,
    TypeOrmModule.forFeature([FeedComment, Feed, User]),
  ],
  controllers: [FeedCommentsController],
  providers: [FeedCommentsService],
  exports: [FeedCommentsModule],
})
export class FeedCommentsModule {}
