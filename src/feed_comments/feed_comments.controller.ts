import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FeedCommentsService } from './feed_comments.service';
import { CreateFeedCommentDto } from './dto/create-feed_comment.dto';
import { UpdateFeedCommentDto } from './dto/update-feed_comment.dto';

@Controller('feed_comments')
export class FeedCommentsController {
  constructor(private readonly feedCommentsService: FeedCommentsService) {}

  @Post()
  create(@Body() createFeedCommentDto: CreateFeedCommentDto) {
    return this.feedCommentsService.create(createFeedCommentDto);
  }

  @Get()
  findAll() {
    return this.feedCommentsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.feedCommentsService.findOne(+id);
  }

  @Get('feed/:feed_id')
  findByFeed(@Param('feed_id') feed_id: number) {
    return this.feedCommentsService.findByFeed(feed_id);
  }

  @Get('user/:user_id')
  findByUser(@Param('user_id') user_id: number) {
    return this.feedCommentsService.findByUser(user_id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFeedCommentDto: UpdateFeedCommentDto,
  ) {
    return this.feedCommentsService.update(+id, updateFeedCommentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.feedCommentsService.remove(+id);
  }
}
