import { IsNotEmpty } from 'class-validator';

export class CreateReportReviewDto {
  @IsNotEmpty()
  rev_id: number;
}
