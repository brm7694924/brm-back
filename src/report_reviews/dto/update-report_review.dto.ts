import { PartialType } from '@nestjs/mapped-types';
import { CreateReportReviewDto } from './create-report_review.dto';

export class UpdateReportReviewDto extends PartialType(CreateReportReviewDto) {}
