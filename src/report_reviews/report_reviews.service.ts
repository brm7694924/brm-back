import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReportReviewDto } from './dto/create-report_review.dto';
import { UpdateReportReviewDto } from './dto/update-report_review.dto';
import { ReportReview } from './entities/report_review.entity';
import { Review } from 'src/reviews/entities/review.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMyfavoriteDto } from 'src/myfavorites/dto/create-myfavorite.dto';
import { Myfavorite } from 'src/myfavorites/entities/myfavorite.entity';
import { Repository } from 'typeorm';
import { UpdateMyfavoriteDto } from 'src/myfavorites/dto/update-myfavorite.dto';

@Injectable()
export class ReportReviewsService {
  constructor(
    @InjectRepository(ReportReview)
    private reportReviewsRepository: Repository<ReportReview>,
    @InjectRepository(Review)
    private reviewsRepository: Repository<Review>,
  ) {}
  async create(createReportReviewDto: CreateReportReviewDto) {
    const review = await this.reviewsRepository.findOne({
      where: { rev_id: createReportReviewDto.rev_id },
    });
    const newReportReview = new ReportReview();
    newReportReview.review = review;
    return this.reportReviewsRepository.save(newReportReview);
  }

  findByReview(reviewId: number) {
    return this.reportReviewsRepository.find({
      where: { review: { rev_id: reviewId } },
      relations: ['review'],
    });
  }

  findAll() {
    return this.reportReviewsRepository.find({ relations: ['review'] });
  }

  findOne(id: number) {
    const reportReview = this.reportReviewsRepository.findOne({
      where: { rep_rev_id: id },
      relations: ['review'],
    });
    if (!reportReview) {
      throw new NotFoundException();
    }
    return this.reportReviewsRepository.findOne({ where: { rep_rev_id: id } });
  }

  async update(id: number, updateReportReviewDto: UpdateReportReviewDto) {
    const reportReview = await this.reportReviewsRepository.findOneBy({
      rep_rev_id: id,
    });
    if (!reportReview) {
      throw new NotFoundException();
    }
    const updatedReportReview = { ...reportReview, ...updateReportReviewDto };
    return this.reportReviewsRepository.save(updatedReportReview);
  }

  async remove(id: number) {
    const reportReview = await this.reportReviewsRepository.findOneBy({
      rep_rev_id: id,
    });
    if (!reportReview) {
      throw new NotFoundException();
    }
    return this.reportReviewsRepository.softRemove(reportReview);
  }
}
