import { Module } from '@nestjs/common';
import { ReportReviewsService } from './report_reviews.service';
import { ReportReviewsController } from './report_reviews.controller';
import { ReportReview } from './entities/report_review.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Review } from 'src/reviews/entities/review.entity';
import { ReviewsModule } from 'src/reviews/reviews.module';

@Module({
  imports: [ReviewsModule, TypeOrmModule.forFeature([ReportReview, Review])],
  controllers: [ReportReviewsController],
  providers: [ReportReviewsService],
  exports: [ReportReviewsModule],
})
export class ReportReviewsModule {}
