import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ReportReviewsService } from './report_reviews.service';
import { CreateReportReviewDto } from './dto/create-report_review.dto';
import { UpdateReportReviewDto } from './dto/update-report_review.dto';

@Controller('report_reviews')
export class ReportReviewsController {
  constructor(private readonly reportReviewsService: ReportReviewsService) {}

  @Post()
  create(@Body() createReportReviewDto: CreateReportReviewDto) {
    return this.reportReviewsService.create(createReportReviewDto);
  }

  @Get()
  findAll() {
    return this.reportReviewsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reportReviewsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateReportReviewDto: UpdateReportReviewDto,
  ) {
    return this.reportReviewsService.update(+id, updateReportReviewDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reportReviewsService.remove(+id);
  }
}
