import { FeedComment } from 'src/feed_comments/entities/feed_comment.entity';
import { Review } from 'src/reviews/entities/review.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class ReportReview {
  @PrimaryGeneratedColumn()
  rep_rev_id: number;

  @ManyToOne(() => Review, (review) => review.reportReview)
  review: Review;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
